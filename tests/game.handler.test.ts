import {Plateau} from "../src/models/plateau.model";
import {Rover} from "../src/models/rover.model";
import {CardinalDirection} from "../src/models/enums/cardinal.direction.enum";
import {InputParserUtil} from "../src/utils/input.parser.util";
import RoverHandler from "../src/core/rover.handler";

describe('Game handler test', () => {
    it('Should create game handler successfully', () => {
        const parser: InputParserUtil = InputParserUtil.getInstance();
        const plateau = new Plateau({X: 5, Y: 5});

        const rover1 = new Rover(plateau, {X: 1, Y: 2}, CardinalDirection.NORTH);
        rover1.setInstructions(parser.parseRoverInstructions("LMLMLMLMM"))

        const rover2 = new Rover(plateau, {X: 3, Y: 3}, CardinalDirection.EAST);
        rover2.setInstructions(parser.parseRoverInstructions("MMRMMRMRRM"))

        const gameHandler: RoverHandler = new RoverHandler(plateau, [rover1, rover2])
        expect(gameHandler.getPlateau()).toEqual(plateau);
        expect(gameHandler.getRovers()).toEqual([rover1, rover2]);
    })

    it('Should execute rover navigation successfully', () => {
        const parser: InputParserUtil = InputParserUtil.getInstance();
        const plateau = new Plateau({X: 5, Y: 5});

        const rover1 = new Rover(plateau, {X: 1, Y: 2}, CardinalDirection.NORTH);
        rover1.setInstructions(parser.parseRoverInstructions("LMLMLMLMM"))

        const rover2 = new Rover(plateau, {X: 3, Y: 3}, CardinalDirection.EAST);
        rover2.setInstructions(parser.parseRoverInstructions("MMRMMRMRRM"))

        const roverHandler: RoverHandler = new RoverHandler(plateau, [rover1, rover2])
        roverHandler.execute();

        expect(roverHandler.getPlateau().isPositionOccupied({Y: 3, X: 1})).toEqual(true);
        expect(roverHandler.getRovers()[0].getCoordinates()).toEqual({Y: 3, X: 1});
        expect(roverHandler.getRovers()[0].getDirection()).toEqual(CardinalDirection.NORTH);

        expect(roverHandler.getPlateau().isPositionOccupied({Y: 1, X: 5})).toEqual(true);
        expect(roverHandler.getRovers()[1].getCoordinates()).toEqual({Y: 1, X: 5});
        expect(roverHandler.getRovers()[1].getDirection()).toEqual(CardinalDirection.EAST);

        expect(roverHandler.getRovers()).toEqual([rover1, rover2]);
    })
})
