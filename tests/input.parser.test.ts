import {InputParserUtil} from "../src/utils/input.parser.util";
import {ParsingError} from "../src/errors/parsing.error";
import {RoverInstruction} from "../src/models/enums/movement.instruction.enum";

describe('Input parser test', () => {
    it('Should throw an error when given incorrect number of coordinates', () => {
        const parser = InputParserUtil.getInstance();

        const op = () => {
            return parser.parsePlateauCoordinates("5  ")
        }
        expect(op).toThrow(ParsingError);
    })

    it('Should throw an error when given invalid coordinates', () => {
        const parser = InputParserUtil.getInstance();

        const op = () => {
            return parser.parsePlateauCoordinates("String String")
        }
        expect(op).toThrow(ParsingError);
    })

    it('Should successfully parse valid co-ordinates', () => {
        const parser = InputParserUtil.getInstance();

        const coordinates = parser.parsePlateauCoordinates("5 5")
        expect(coordinates).toEqual({X: 5, Y: 5});
    })

    it('Should throw error when given invalid rover input', () => {
        const parser = InputParserUtil.getInstance();

        const op1 = () => parser.parseRoverInput("555555")
        const op2 = () => parser.parseRoverInput("5 5 5 5")
        const op3 = () => parser.parseRoverInput("5 5 5")
        const op4 = () => parser.parseRoverInput("")
        const op5 = () => parser.parseRoverInput("5 5 R")

        expect(op1).toThrow(ParsingError);
        expect(op2).toThrow(ParsingError);
        expect(op3).toThrow(ParsingError);
        expect(op4).toThrow(ParsingError);
        expect(op5).toThrow(ParsingError);
    })

    it('Should successfully parse valid rover values', () => {
        const parser = InputParserUtil.getInstance();

        const roverValues: any[] = parser.parseRoverInput("5 5 E")
        expect(roverValues[0]).toEqual(5);
        expect(roverValues[1]).toEqual(5);
        expect(roverValues[2]).toEqual('E');
    })

    it('Should throw error if given invalid rover instructions', () => {
        const parser = InputParserUtil.getInstance();
        const op = () => parser.parseRoverInstructions("Q")
        const op1 = () => parser.parseRoverInstructions("1234")
        const op2 = () => parser.parseRoverInstructions("LMLMLMRPZ")
        const op3 = () => parser.parseRoverInstructions("")

        expect(op).toThrow(ParsingError);
        expect(op1).toThrow(ParsingError);
        expect(op2).toThrow(ParsingError);
        expect(op3).toThrow(ParsingError);
    })

    it('Should successfully return instructions when given valid rover instructions', () => {
        const parser = InputParserUtil.getInstance();

        const instructions: RoverInstruction[] = parser.parseRoverInstructions("LMLMLMLMM")

        expect(instructions.length).toEqual(9);
        expect(instructions.join("").toString()).toEqual("LMLMLMLMM");
    })
})
