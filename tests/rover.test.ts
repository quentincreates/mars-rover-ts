import {Plateau} from "../src/models/plateau.model";
import {InitializationError} from "../src/errors/initialization.error";
import {Rover} from "../src/models/rover.model";
import {CardinalDirection} from "../src/models/enums/cardinal.direction.enum";
import {RoverInstruction} from "../src/models/enums/movement.instruction.enum";

describe('Rover test', () => {

    it('Should throw error if given coordinates that do not exist on the plateau', () => {
        const op = () => {
            const plateau = new Plateau({X: 20, Y: 20});
            return new Rover(plateau, {X: 30, Y: 30}, CardinalDirection.NORTH);
        };
        expect(op).toThrow(InitializationError);
    })

    it('Should throw error if given coordinates are occupied on the plateau', () => {
        const op = () => {
            const plateau = new Plateau({X: 20, Y: 20});

            plateau.setPositionOccupation({X: 10, Y: 10}, true);
            return new Rover(plateau, {X: 10, Y: 10}, CardinalDirection.NORTH);
        };
        expect(op).toThrow(InitializationError);
    })

    it('Should create rover and set occupation on plateau', () => {
        const plateau = new Plateau({X: 20, Y: 20});

        new Rover(plateau, {X: 10, Y: 10}, CardinalDirection.NORTH);
        expect(plateau.isPositionOccupied({X: 10, Y: 10})).toBe(true);
    })

    it('Should create rover and set cardinal direction on plateau', () => {
        const plateau = new Plateau({X: 20, Y: 20});

        const rover = new Rover(plateau, {X: 10, Y: 10}, CardinalDirection.NORTH);
        expect(rover.getDirection()).toBe(CardinalDirection.NORTH);
    })

    it('Should rotate successfully when given instruction', () => {
        const plateau = new Plateau({X: 20, Y: 20});

        const rover = new Rover(plateau, {X: 10, Y: 10}, CardinalDirection.NORTH);
        rover.rotateRover(RoverInstruction.R)
        expect(rover.getDirection()).toBe(CardinalDirection.EAST);
    })

    it('Should rotate to NORTH successfully from WEST when the given instruction "R"', () => {
        const plateau = new Plateau({X: 20, Y: 20});

        const rover = new Rover(plateau, {X: 10, Y: 10}, CardinalDirection.WEST);
        rover.rotateRover(RoverInstruction.R)
        expect(rover.getDirection()).toBe(CardinalDirection.NORTH);
    })

    it('Should rotate to WEST successfully from NORTH when the given instruction "L"', () => {
        const plateau = new Plateau({X: 20, Y: 20});

        const rover = new Rover(plateau, {X: 10, Y: 10}, CardinalDirection.NORTH);
        rover.rotateRover(RoverInstruction.L)
        expect(rover.getDirection()).toBe(CardinalDirection.WEST);
    })

    it('Should move rover NORTH successfully when given the "M" instruction', () => {
        const plateau = new Plateau({X: 20, Y: 20});

        const rover = new Rover(plateau, {X: 10, Y: 10}, CardinalDirection.NORTH);
        rover.moveRover(plateau)
        expect(rover.getCoordinates()).toEqual({X: 10, Y: 11});
    })

    it('Should move rover EAST successfully when given the "M" instruction', () => {
        const plateau = new Plateau({X: 20, Y: 20});

        const rover = new Rover(plateau, {X: 10, Y: 10}, CardinalDirection.EAST);
        rover.moveRover(plateau)
        expect(rover.getCoordinates()).toEqual({X: 11, Y: 10});
    })

    it('Should move rover SOUTH successfully when given the "M" instruction', () => {
        const plateau = new Plateau({X: 20, Y: 20});

        const rover = new Rover(plateau, {X: 10, Y: 10}, CardinalDirection.SOUTH);
        rover.moveRover(plateau)
        expect(rover.getCoordinates()).toEqual({X: 10, Y: 9});
    })

    it('Should move rover WEST successfully when given the "M" instruction', () => {
        const plateau = new Plateau({X: 20, Y: 20});

        const rover = new Rover(plateau, {X: 10, Y: 10}, CardinalDirection.WEST);
        rover.moveRover(plateau)
        expect(rover.getCoordinates()).toEqual({X: 9, Y: 10});
    })

})
