import 'jest'
import {Plateau} from "../src/models/plateau.model";
import {InitializationError} from "../src/errors/initialization.error";

describe('Plateau tests', () => {

    it('Should throw error if given small plateau', () => {
        const op = () => {
            new Plateau({X: 0, Y: 0});
        };
        expect(op).toThrow(InitializationError);
    })

    it('Should throw error if given negative co-ordinates for the plateau', () => {
        const op = () => {
            new Plateau({X: -10, Y: -2});
        };
        expect(op).toThrow(InitializationError);
    })

    it('Should initialize the plateau successfully', () => {
        const plateau = new Plateau({X: 5, Y: 5});
        const size = plateau.getGrid().length - 1;

        expect(size).toEqual(5);
        expect(plateau.getGrid()[0][0].X).toEqual(0);
        expect(plateau.getGrid()[0][0].Y).toEqual(0);

        expect(plateau.getGrid()[5][5].X).toEqual(5);
        expect(plateau.getGrid()[5][5].Y).toEqual(5);
    })

    it('Should return true if given valid plateau position', () => {
        const plateau = new Plateau({X: 10, Y: 10});

        expect(plateau.isValidPosition({X: 5, Y: 5})).toBe(true);
        expect(plateau.isValidPosition({X: 10, Y: 10})).toBe(true);
    })

    it('Should return false if given invalid plateau position', () => {
        const plateau = new Plateau({X: 5, Y: 5});

        expect(plateau.isValidPosition({X: 6, Y: 6})).toBe(false);
        expect(plateau.isValidPosition({X: 20, Y: 5})).toBe(false);
        expect(plateau.isValidPosition({X: 5, Y: 20})).toBe(false);
    })

    it('Should return true if position is occupied', () => {
        const plateau = new Plateau({X: 20, Y: 20});

        plateau.setPositionOccupation({X: 5, Y: 2}, true);
        plateau.setPositionOccupation({X: 20, Y: 16}, true);

        expect(plateau.isPositionOccupied({X: 5, Y: 2})).toBe(true);
        expect(plateau.isPositionOccupied({X: 20, Y: 16})).toBe(true);
    })

    it('Should return false if position is not occupied', () => {
        const plateau = new Plateau({X: 20, Y: 20});

        expect(plateau.isPositionOccupied({X: 5, Y: 2})).toBe(false);
        expect(plateau.isPositionOccupied({X: 30, Y: 30})).toBe(false);
    })
})
