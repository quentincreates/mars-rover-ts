import {ParsingError} from "../errors/parsing.error";
import {CardinalDirection} from "../models/enums/cardinal.direction.enum";
import {Coordinates} from "../models/interfaces/coordinates.interface";
import {RoverInstruction} from "../models/enums/movement.instruction.enum";

export class InputParserUtil {
    private static instance: InputParserUtil = new InputParserUtil();

    private constructor() {
        //prevents instantiation
    }

    public static getInstance() {
        return this.instance;
    }

    public parsePlateauCoordinates = (input: string): Coordinates => {
        let split = input.split(" ");

        if (split.length !== 2)
            throw new ParsingError(`Incorrect number of plateau coordinates provided. Required [2]`);

        if (!this.isNumber(+split[0]) || !this.isNumber(+split[1]))
            throw new ParsingError(`Incorrect plateau coordinates provided. Two positive integers required, i.e "5 5"`);

        return {X: +split[0], Y: +split[1]};
    }

    public parseRoverInput = (input: string): any[] => {
        let split = input.split(" ");

        if (split.length !== 3)
            throw new ParsingError(`Error experienced when parsing rover input, provide valid input, i.e "5 1 E"`);

        const roverCoordinates: Coordinates = this.parsePlateauCoordinates(split[0] + " " + split[1]);

        if (!this.isValidCardinalDirection(split[2]))
            throw new ParsingError(`Error experienced when parsing rover cardinal direction, provide valid input`);

        const values: any[] = [roverCoordinates.X, roverCoordinates.Y, split[2]];
        return values;
    }

    public parseRoverInstructions = (input: string): RoverInstruction[] => {
        const split = input.split("")

        if (!split.length)
            throw new ParsingError(`Error experienced when parsing rover movement input, provide valid input, i.e "LMLMLMLMM"`);

        let instructions: RoverInstruction[] = [];
        for (let i = 0; i < split.length; i++) {
            if (split[i] !== 'L' && split[i] !== 'M' && split[i] !== 'R')
                throw new ParsingError(`Error experienced when parsing rover movement [${split[i]}] input, please provide invalid input`);
            instructions.push(<RoverInstruction>split[i]);
        }
        return instructions;
    }

    private isNumber = (value: string | number): boolean => {
        return ((value != null) &&
            (value !== '') &&
            !isNaN(Number(value.toString())));
    }

    private isValidCardinalDirection = (value: string): boolean => {
        const values = Object.values(CardinalDirection);
        for (let i = 0; i < values.length; i++) {
            if (values[i] == value)
                return true;
        }
        return false;
    }
}
