"use strict";
import {Rover} from "./models/rover.model";
import {InputHandler} from "./core/input.handler";
import RoverHandler from "./core/rover.handler";

//Get user input and setup RoverHandler
const handler: InputHandler = new InputHandler();
const plateau = handler.getPlateau();
const rovers: Rover[] = handler.getRovers(plateau);

const gameHandler: RoverHandler = new RoverHandler(plateau, rovers);
gameHandler.execute();

console.log(`\nRover 1 final position [${gameHandler.getRovers()[0].getCoordinates().X} ${gameHandler.getRovers()[0].getCoordinates().Y} ${gameHandler.getRovers()[0].getDirection()}]`);
console.log(`Rover 2 final position [${gameHandler.getRovers()[1].getCoordinates().X} ${gameHandler.getRovers()[1].getCoordinates().Y} ${gameHandler.getRovers()[1].getDirection()}]\n`);
