export class ParsingError extends Error {
    __proto__ = Error;

    constructor(message: string) {
        super(message);
        Object.setPrototypeOf(this, ParsingError.prototype);//TODO: Check out JS prototypes
    }
}
