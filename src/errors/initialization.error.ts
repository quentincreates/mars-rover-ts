export class InitializationError extends Error {
    __proto__ = Error;

    constructor(message: string) {
        super(message);
        Object.setPrototypeOf(this, InitializationError.prototype);//TODO: Check out JS prototypes
    }
}
