import * as readline from 'readline-sync';
import {Plateau} from "../models/plateau.model";
import {InputParserUtil} from "../utils/input.parser.util";
import {Coordinates} from "../models/interfaces/coordinates.interface";
import {Rover} from "../models/rover.model";

export class InputHandler {
    parser: InputParserUtil = InputParserUtil.getInstance();

    public constructor() {
    }

    public getPlateau = (): Plateau => {
        const input = readline.question(`Enter Plateau [X Y]: `)

        const coordinates: Coordinates = this.parser.parsePlateauCoordinates(input);
        const plateau = new Plateau(coordinates);

        return plateau
    }

    public getRovers = (plateau: Plateau): Rover[] => {
        let rovers: Rover[] = [];
        let isEnd = false;

        let roverCount = 1;
        while (!isEnd) {
            const roverInput = readline.question(`Enter coordinates and cardinal direction for Rover ${roverCount}: `)
            let roverValues: any[] = this.parser.parseRoverInput(roverInput);

            const rover: Rover = new Rover(plateau, {X: roverValues[0], Y: roverValues[1]}, roverValues[2]);

            const rover1MovementInput = readline.question(`Enter rover movement: `)
            const movement = this.parser.parseRoverInstructions(rover1MovementInput);
            rover.setInstructions(movement);

            rovers.push(rover);
            if (roverCount == 2)
                isEnd = true;

            roverCount++;
        }

        return rovers;
    }
}
