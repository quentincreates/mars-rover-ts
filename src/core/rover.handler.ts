import {Plateau} from "../models/plateau.model";
import {Rover} from "../models/rover.model";
import {RoverInstruction} from "../models/enums/movement.instruction.enum";

export default class RoverHandler {
    plateau: Plateau;
    rovers: Rover[];

    public constructor(plateau: Plateau, rovers: Rover[]) {
        this.plateau = plateau;
        this.rovers = rovers;
    }

    public execute = (): boolean => {
        this.rovers.forEach(rover => {

            //Iterate through rover movement instruction
            rover.getInstructions().forEach(instruction => {
                if (instruction == RoverInstruction.R || instruction == RoverInstruction.L) {
                    rover.rotateRover(instruction);
                } else if (instruction == RoverInstruction.M) {
                    rover.moveRover(this.plateau);
                }
            })
        })

        return true;
    }

    public getPlateau = (): Plateau => this.plateau;

    public getRovers = (): Rover[] => this.rovers;
}
