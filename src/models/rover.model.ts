import {Plateau} from "./plateau.model";
import {CardinalDirection} from "./enums/cardinal.direction.enum";
import {Coordinates} from "./interfaces/coordinates.interface";
import {InitializationError} from "../errors/initialization.error";
import {RoverInstruction} from "./enums/movement.instruction.enum";

export class Rover {
    coordinates: Coordinates;
    cardinalDirection: CardinalDirection
    private instructions: RoverInstruction[];

    constructor(plateau: Plateau, coordinates: Coordinates, cardinalDirection: CardinalDirection) {
        if (!plateau.isValidPosition(coordinates))
            throw new InitializationError('Rover coordinates do not exist on the plateau')

        this.coordinates = coordinates;
        this.cardinalDirection = cardinalDirection;
        this.instructions = [];

        if (plateau.isPositionOccupied(coordinates)) {
            throw new InitializationError('Rover coordinates are occupied on the plateau')
        } else {
            plateau.setPositionOccupation(coordinates, true);
        }
    }

    public moveRover = (plateau: Plateau) => {
        let coordinates = this.coordinates;

        switch (this.getDirection()) {
            case CardinalDirection.WEST:
                coordinates = {X: this.coordinates.X - 1, Y: this.coordinates.Y}
                break;
            case CardinalDirection.EAST:
                coordinates = {X: this.coordinates.X + 1, Y: this.coordinates.Y}
                break;
            case CardinalDirection.SOUTH:
                coordinates = {X: this.coordinates.X, Y: this.coordinates.Y - 1};
                break;
            case CardinalDirection.NORTH:
                coordinates = {X: this.coordinates.X , Y: this.coordinates.Y + 1}
                break;
            default:
                break
        }

        if (plateau.isValidPosition(coordinates)) {
            if (!plateau.isPositionOccupied(coordinates)) {
                plateau.setPositionOccupation(this.coordinates, false);
                this.coordinates = coordinates;

                plateau.setPositionOccupation(coordinates, true);
            } else {
                console.log(`Rover cannot move to X: ${this.coordinates.X}, Y: ${this.coordinates.Y + 1} because the position is occupied, ignoring instruction`)
            }
        }
    }

    public rotateRover = (instruction: RoverInstruction) => {
        const cardinals = ['N', 'E', 'S', 'W'];

        let index: number = cardinals.indexOf(this.cardinalDirection);
        (instruction == RoverInstruction.L) ? index-- : index++;

        //wrap around
        if (index > cardinals.length - 1) {
            index = 0;
        } else if (index < 0) {
            index = cardinals.length - 1
        }

        this.setDirection(<CardinalDirection>cardinals[index]);
    }

    public getInstructions = (): RoverInstruction[] => {
        return this.instructions;
    }

    public setInstructions = (instructions: RoverInstruction[]) => {
        this.instructions = instructions;
    }

    public setDirection = (direction: CardinalDirection) => {
        this.cardinalDirection = direction;
    }

    public getDirection = (): CardinalDirection => {
        return this.cardinalDirection;
    }

    public getCoordinates = (): Coordinates => {
        return this.coordinates;
    }
}
