export enum CardinalDirection {
    NORTH = 'N',
    WEST = 'W',
    EAST = 'E',
    SOUTH = 'S'
}
