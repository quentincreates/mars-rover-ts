export enum RoverInstruction {
    L = 'L',
    R = 'R',
    M = 'M'
}
