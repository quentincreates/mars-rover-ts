import {Position} from "./position.model";
import {InitializationError} from "../errors/initialization.error";
import {Coordinates} from "./interfaces/coordinates.interface";

export class Plateau {
    private grid: Position[][] = [];

    constructor(coordinates: Coordinates) {
        if ((coordinates.Y === 0 && coordinates.X === 0) || coordinates.Y < 0 || coordinates.X < 0) {
            throw new InitializationError('Provided plateau is too small')
        }

        //initialize grid
        for (let i: number = 0; i < coordinates.X + 1; i++) {
            this.grid[i] = [];

            for (let j: number = 0; j < coordinates.Y + 1; j++) {
                this.grid[i][j] = new Position(j, i);
            }
        }
    }

    public isPositionOccupied = (coordinates: Coordinates): boolean => {
        if (this.isValidPosition(coordinates))
            return this.grid[coordinates.X][coordinates.Y].isOccupied;

        return false;
    }

    public setPositionOccupation = (coordinates: Coordinates, isOccupied: boolean) => {
        if (this.isValidPosition(coordinates))
            this.grid[coordinates.X][coordinates.Y].isOccupied = isOccupied;
    }

    public isValidPosition = (coordinates: Coordinates): boolean => {
        if (this.grid[coordinates.X] == undefined)
            return false;

        if (this.grid[coordinates.X][coordinates.Y] == undefined)
            return false

        return true;
    }

    public getGrid = (): Position[][] => this.grid;

}

