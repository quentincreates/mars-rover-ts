import {Coordinates} from "./interfaces/coordinates.interface";

export class Position implements Coordinates {
    X: number;
    Y: number;
    isOccupied: boolean;

    constructor(x: number, y: number) {
        this.X = x;
        this.Y = y;
        this.isOccupied = false;
    }
}
