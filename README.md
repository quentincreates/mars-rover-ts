# Mars Rover

A squad of robotic rovers are to be landed by NASA on a plateau on Mars.
This plateau, which is curiously rectangular, must be navigated by the rovers so that their on board cameras can get a complete view of the surrounding terrain to send back to Earth.

A rover's position is represented by a combination of x and y coordinates and a letter representing one of the four cardinal compass points.

The plateau is divided up into a grid to simplify navigation. An example position might be 0, 0, N, which means the rover is in the bottom left corner facing North.

In order to control a rover, NASA sends a simple string of letters. The possible letters are 'L', 'R' and 'M'.

'L' and 'R' makes the rover spin 90 degrees left or right respectively, without moving from its current spot. 'M' means move forward one grid point, and maintain the same heading.

### Requirements
#### Software
* Typescript 4.6.4
* NPM (Node package manager) 8.5
* Node v17

## How to(s)
* ####  Install required npm packages

```bash
$> npm install
```

* #### Run Mars Rover project

```bash
$> npm start
```

* #### Run test suites

```bash
$> npm run test
```

## Input
```shell
  Enter Plateau [X Y]:
  Enter coordinates and cardinal direction for Rover 1:
  Enter rover movement:
  
  Enter coordinates and cardinal direction for Rover 2:
  Enter rover movement:
```
## Output
```shell
    Rover 1 final position [1 3 N]
    Rover 2 final position [5 1 E]
```

#### Rover & plateau constraints
* Rover:
  * Cannot be placed or move to a position that is already occupied on the plateau
  * Cannot be placed on a position that does not exist on the plateau
    
    
* Plateau:
  * Cannot be given a small size. i.e "0 0" 
